# [Sudoku Solver](https://www.codingame.com/training/medium/sudoku-solver)
[Exercise by By AllTheKingsMen](https://www.codingame.com/profile/571927d715f15c3dec7693f461e2a63c6671233)

### Goal
Your program must output a solution to a 9x9 sudoku. A sudoku is a Latin Square which has the numbers 1-9 in each row, column, and 3x3 square.

### Input
First 9 lines: A string `line` with 9 digits corresponding to that line. A 0 is used for an empty space.

### Output
First 9 lines: A string `line` with 9 digits corresponding to that line. The original numbers should not have changed, and there should be no zeros left.

### Example
```
Input

120070560
507932080
000001000
010240050
308000402
070085010
000700000
080423701
034010028
```
```
Output

123874569
567932184
849651237
916247853
358196472
472385916
291768345
685423791
734519628
```
