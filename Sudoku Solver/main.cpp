#include <iostream>
#include <vector>

using namespace std;

bool verify(string& sudoku, const unsigned int& pos) {
    const unsigned int x = pos % 9;
    const unsigned int y = pos / 9;

    const unsigned int sq_x = x / 3;
    const unsigned int sq_y = y / 3;

    vector<unsigned int> positions_to_verify(3);

    for (unsigned int i = 0; i < 9; i++) {
        // verify line
        positions_to_verify[0] = i + y * 9;
        // verify column
        positions_to_verify[1] = i * 9 + x;
        // verify 3x3 square
        positions_to_verify[2] = (sq_y * 3 + i / 3) * 9 + (sq_x * 3 + i % 3);
        for (auto& j : positions_to_verify) {
            if (j != pos && sudoku[j] == sudoku[pos]) return false;
        }
    }
    return true;
}

bool solve(string& sudoku, const unsigned int pos) {
    unsigned int next_0 = sudoku.find('0', pos);

    if (next_0 == (unsigned int) string::npos) return true;

    for (char n = '1'; n <= '9'; n++) {
        sudoku[next_0] = n;
        if (!verify(sudoku, next_0)) continue;
        if (solve(sudoku, next_0 + 1)) return true;
    }
    sudoku[next_0] = '0';
    return false;
}

int main()
{
    string sudoku;

    // Loop to get sudoku map lines from input
    for (int i = 0; i < 9; i++) {
        string line;
        getline(cin, line);
        sudoku.append(line);
    }

    if (!solve(sudoku, 0))
    {
        cout << "No solution found" << endl;
    } else {
        for (unsigned long i = 0; i < 9; i++) {
            cout << sudoku.substr(i * 9, 9) << endl;
        }
    }
}
