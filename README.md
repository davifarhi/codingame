# [CodinGame](https://www.codingame.com/home)

[My Profile](https://www.codingame.com/profile/6692e375449a304fb1d4dd628e04954d7193265)

This repository contains my solutions to puzzles and exercises from CodinGame.com
